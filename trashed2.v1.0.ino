// ===========================================
// trashed2 (v2.0)
// Jan 12, 2020

// Nicole Vella
// www.nicolevella.com

// Materials used:
// - arduino uno
// - 10mm RGB Led (x2)
// - low-density polyethylene sourced from a trashcan

// Steal this code but give me credit pls
// ===========================================

// two RGB 10mm LEDs
const int redLed    = 11;
const int greenLed  = 10;
const int blueLed   = 9;
const int redLed2    = 6;
const int greenLed2  = 5;
const int blueLed2   = 3;

void setup() {
  pinMode(redLed, OUTPUT);
  pinMode(greenLed, OUTPUT);
  pinMode(blueLed, OUTPUT);
  pinMode(redLed2, OUTPUT);
  pinMode(greenLed2, OUTPUT);
  pinMode(blueLed2, OUTPUT);
}

void loop() {
  colorCycle(75);
}

void colorCycle(int fadeSpeed) {
  // A function that cycles through the colour spectrum in order of red -> green -> blue.
  // By overlapping the fades, we can mix the rgb values within the LED and create a full spectrum fade.
  // Takes one parameter, the fade speed in ms.

  analogWrite(greenLed, 255); // we start with green on high so that the cycle loops seamlessly
  analogWrite(greenLed2, 255);

  for (int i = 0; i <= 255; i++) {  // turn up red
    analogWrite(redLed, i);
    analogWrite(redLed2, i);
    delay(fadeSpeed);
  }

  for (int i = 255; i >= 0; i--) {  // turn down green
    analogWrite(greenLed, i);
    analogWrite(greenLed2, i);
    delay(fadeSpeed);
  }

  for (int i = 0; i <= 255; i++) { // turn up blue
    analogWrite(blueLed, i);
    analogWrite(blueLed2, i);
    delay(fadeSpeed);
  }

  for (int i = 255; i >= 0; i--) { // turn down red
    analogWrite(redLed, i);
    analogWrite(redLed2, i);
    delay(fadeSpeed);
  }

  for (int i = 0; i <= 255; i++) {  // turn up green
    analogWrite(greenLed, i);
    analogWrite(greenLed2, i);
    delay(fadeSpeed);
  }

  for (int i = 255; i >= 0; i--) { // turn down blue
    analogWrite(blueLed, i);
    analogWrite(blueLed2, i);
    delay(fadeSpeed);
  }
}
