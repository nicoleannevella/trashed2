# trashed<sup>2</sup>

**Trashed<sup>2</sup> is cubed light that cycles through the full spectrum of colours.**

As an exploration in repurposing packaging and shipping materials, I wanted to create something interesting out of a material that is typically an afterthought. Low-density polyethylene (LDPE) or low-density Styrofoam<sup>&trade;</sup> is often used as packing/shipping material and then discarded to a landfill where it will take up to one million years to fully decompose. It has characteristics that make it an ideal medium to create things. LDPE has structural integrity and can be used as "bricks" to build large exhibits. It is also extremely lightweight, insulating, and softly diffuses light perfectly.

In the future I hope to add interactivity to this piece such as:
*  Pausing on the current colour when the user taps the cube.
*  Adding a dial or slide input to control the speed of the colour fade.
*  Interaction with audio and music.
*  Adding wifi/bluetooth capabilities so that additional cubes can interact with one another.



<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Trashed<sup>2</sup></span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.nicolevella.com" property="cc:attributionName" rel="cc:attributionURL">Nicole Vella</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.<br><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a>